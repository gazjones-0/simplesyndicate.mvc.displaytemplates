﻿SimpleSyndicate.Mvc.DisplayTemplates NuGet package.

Common display templates for ASP.Net MVC applications:
- Nullable<DateTime>

The display templates have been copied to your Views\Shared\DisplayTemplates folder.
