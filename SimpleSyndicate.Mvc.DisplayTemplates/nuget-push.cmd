@echo off
rem check nugetutil is available
if not exist ..\tools\SimpleSyndicate.NuGetUtil.exe (
	echo Fatal: ..\tools\SimpleSyndicate.NuGetUtil.exe not found
	goto :end
)

rem check input argument
for /F "delims=" %%i in ('..\tools\SimpleSyndicate.NuGetUtil.exe checkversionupdatearg %1') do set nugetutil=%%i
set nugetutilprefix=%nugetutil:~0,5%
if /i "%nugetutilprefix%" == "Valid" (
	goto :precheck
)
echo Fatal: No version update component specified
echo Usage: nuget-push ^<major^|minor^|point^|prerelease^> [path]
echo   If path is specified, the package will not be pushed, but instead
echo   copied to the specified path.
goto :end

:precheck
if /i not "%1"=="prerelease" (
	echo Release checklist:
	echo 1. Removed and sorted usings?
	echo 2. Performed code analysis?
	echo 3. Added a new version history item to help file project?
	echo 4. Built help file to ensure it has no errors or warnings?
	choice /m "Checklist completed? [Y]es or [N]o?"
	if errorlevel 2 goto :end
)

:checkTools
rem check Visual Studio is available
if not defined VSINSTALLDIR (
	echo Fatal: Visual Studio not found
	goto :end
)

rem check nuget is available
for /F "delims=" %%i in ('nuget') do set nugetversion=%%i
if not defined nugetversion (
	echo Fatal: nuget not found
	goto :end
)

rem check git is available
for /F "delims=" %%i in ('git --version') do set gitversion=%%i
if not defined gitversion (
	echo Fatal: git not found
	goto :end
)

rem if %home% isn't defined, set it to the home drive and path
if not defined HOME (
	set HOME=%HOMEDRIVE%%HOMEPATH%
)

rem if the home drive and path aren't defined, home will still be undefined so set it to the user profile
if not defined HOME (
	set HOME=%USERPROFILE%
)

rem sanity check %home% is defined, otherwise git won't be able to find its global config
if not defined HOME (
	echo Fatal: No HOME environment variable defined
	echo Fatal: Without this git won't be able to find its global config
	goto :end
)

:push
rem update version
..\tools\SimpleSyndicate.NuGetUtil versionupdatenoreleasenotes %1

rem update dependencies
..\tools\SimpleSyndicate.NuGetUtil dependenciesupdate

rem build the solution
pushd .
cd ..
msbuild /m /nologo /verbosity:quiet /p:Configuration=Release
popd

rem create a sub-folder to perform the packaging as we want to customise the files in it
pushd .
mkdir .nuget
copy *.nuspec .nuget
cd .nuget

rem build release package and push to nuget
nuget pack SimpleSyndicate.Mvc.DisplayTemplates.nuspec
if /i "%~2"=="" (
	nuget push *.nupkg
) else (
	copy *.nupkg %2
)

rem build symbols package and push to nuget
nuget pack SimpleSyndicate.Mvc.DisplayTemplates.Symbols.nuspec -Symbols
if /i "%~2"=="" (
	nuget push *.symbols.nupkg
) else (
	copy *.symbols.nupkg %2
)

rem back to original directory
popd

rem clean up packaging folder
rmdir /s /q .nuget

rem build the documentation, but only if we're not doing a pre-release build for faster iteration cycles
if /i not "%1"=="prerelease" (
	pushd .
	cd ..\..\SimpleSyndicate.Help
	msbuild /m /nologo /verbosity:quiet /p:Configuration=Release
	rmdir /s /q SimpleSyndicate.Help\Help\fti
	rmdir /s /q SimpleSyndicate.Help\Help\html
	rmdir /s /q SimpleSyndicate.Help\Help\icons
	rmdir /s /q SimpleSyndicate.Help\Help\scripts
	rmdir /s /q SimpleSyndicate.Help\Help\styles
	rmdir /s /q SimpleSyndicate.Help\Help\toc
	popd
)

rem work out the git tags and messages
for /F "delims=" %%i in ('..\tools\SimpleSyndicate.NuGetUtil currentversion') do set currentversion=%%i
set tag=%currentversion%
if /i "%~2"=="" (
	set message=Set version to %currentversion%; pushed to NuGet.
) else (
	set message=Set version to %currentversion%; files copied (package not pushed to NuGet^).
)
set helptag=SimpleSyndicate.Mvc.DisplayTemplates_%currentversion%
set helpmessage=Documentation updated for SimpleSyndicate.Mvc.DisplayTemplates %currentversion%.

rem commit the changes for this version
pushd .
cd ..
git add --all *
git commit -a -m "%message%"
git tag -a %tag% -m "%message%"
git push --all
git push --tags
popd

rem commit the updated documentation solution, but only if we're not doing a pre-release build
if /i not "%1"=="prerelease" (
	pushd .
	cd ..\..\SimpleSyndicate.Help
	git add --all *
	git commit -a -m "%helpmessage%"
	git tag -a %helptag% -m "%helpmessage%"
	git push --all
	git push --tags
	popd
)

rem commit the updated documentation, but only if we're not doing a pre-release build
if /i not "%1"=="prerelease" (
	pushd .
	cd ..\..\gazooka_g72.bitbucket.org
	git add --all *
	git commit -a -m "%helpmessage%"
	git tag -a %helptag% -m "%helpmessage%"
	git push --all
	git push --tags
	popd
)

:end
