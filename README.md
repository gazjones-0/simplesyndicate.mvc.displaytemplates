# README #

SimpleSyndicate.Mvc.DisplayTemplates NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Mvc.DisplayTemplates

### What is this repository for? ###

Common display templates for ASP.Net MVC applications:

* Nullable<DateTime>

### How do I get set up? ###

* Intall the NuGet package and the display templates will be copied into your project.

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.mvc.displaytemplates/issues?status=new&status=open
